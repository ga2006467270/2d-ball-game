﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
    [SerializeField] GameObject mReadMePanel;

    public void GameStart()
    {
        SceneManager.LoadScene("PlayScene", LoadSceneMode.Single);
    }

    public void OpenReadMe()
    {
        mReadMePanel.SetActive(true);
    }

    public void CloseReadMe()
    {
        mReadMePanel.SetActive(false);
    }
}
