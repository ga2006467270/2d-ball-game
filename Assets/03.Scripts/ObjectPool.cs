﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("Main");
                gamemanager.AddComponent<ObjectPool>();
            }
            return mInstance;
        }
    }

    static ObjectPool mInstance;
    [SerializeField] GameObject[] mObjectPool;
    Dictionary<string, GameObject> mObjectPoolDic = new Dictionary<string, GameObject>();
    Dictionary<string, GameObject> mObjectInGameDic = new Dictionary<string, GameObject>();

    void Awake()
    {
        if (mInstance == null)
        {
            mInstance = this;
        }
        else
        {
            Debug.LogError("Reapeat Singleton");
            Destroy(gameObject);
        }
    }

    public void SetObjectPool()
    {
        BuildObject();
    }

    public void RespawnGame()
    {
        if (mObjectInGameDic.ContainsKey("Ball"))
        {
            Destroy(mObjectInGameDic["Ball"]);
            mObjectInGameDic.Remove("Ball");
        }
        if (mObjectInGameDic.ContainsKey("Player1"))
        {
            Destroy(mObjectInGameDic["Player1"]);
            mObjectInGameDic.Remove("Player1");
        }
        if (mObjectInGameDic.ContainsKey("Player2"))
        {
            Destroy(mObjectInGameDic["Player2"]);
            mObjectInGameDic.Remove("Player2");
        }
        GameObject ball = Instantiate(mObjectPoolDic["Ball"]);
        mObjectInGameDic.Add(mObjectPoolDic["Ball"].name, ball);

        GameObject player1 = Instantiate(mObjectPoolDic["Player1"]);
        mObjectInGameDic.Add(mObjectPoolDic["Player1"].name, player1);

        GameObject player2 = Instantiate(mObjectPoolDic["Player2"]);
        mObjectInGameDic.Add(mObjectPoolDic["Player2"].name, player2);
    }

    void BuildObject()
    {
        foreach (GameObject obj in mObjectPool)
        {
            mObjectPoolDic.Add(obj.name, obj);
            GameObject objectingame = Instantiate(obj);
            mObjectInGameDic.Add(obj.name, objectingame);
        }
    }
}
