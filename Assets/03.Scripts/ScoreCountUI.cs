﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCountUI : MonoBehaviour
{
    private static ScoreCountUI mInstance;
    [SerializeField] Text mRaceTimeText;
    [SerializeField] Text mScoreText;
    [SerializeField] Text mGoalUI;
    [SerializeField] Text mStartUI;
    float mColorAlphaCounter;

    public static ScoreCountUI Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("Main");
                gamemanager.AddComponent<ScoreCountUI>();
            }
            return mInstance;
        }
    }

    // Use this for initialization
    void Awake()
    {
        if (mInstance == null)
        {
            mInstance = this;
        }
        else
        {
            Debug.LogError("Reapeat Singleton");
            Destroy(gameObject);
        }
    }

    public IEnumerator ShowGoalText()
    {
        mColorAlphaCounter = 1;
        mGoalUI.color = new Color(mGoalUI.color.r, mGoalUI.color.g, mGoalUI.color.b, mColorAlphaCounter);
        yield return new WaitForSeconds(1f);
        StartCoroutine(FadeGoalText());
    }

    IEnumerator FadeGoalText()
    {
        mColorAlphaCounter -= Time.deltaTime;
        yield return new WaitForFixedUpdate();
        mGoalUI.color = new Color(mGoalUI.color.r, mGoalUI.color.g, mGoalUI.color.b, mColorAlphaCounter);
        if (mColorAlphaCounter >= 0)
        {
            StartCoroutine(FadeGoalText());
        }
        else
        {
            ObjectPool.Instance.RespawnGame();
            ScoreCounter.Instance.StopTime = false;
            mColorAlphaCounter = 1;
            StartCoroutine(ShowStartText());
        }
    }

    IEnumerator ShowStartText()
    {
        mColorAlphaCounter -= Time.deltaTime;
        yield return new WaitForFixedUpdate();
        mStartUI.color = new Color(mStartUI.color.r, mStartUI.color.g, mStartUI.color.b, mColorAlphaCounter);
        if (mColorAlphaCounter >= 0)
        {
            StartCoroutine(ShowStartText());
        }
    }

    public void ChangeScoreUI(int teamAScore,int teamBScore)
    {
        mScoreText.text = teamAScore + "-" + teamBScore;
    }

    public void SetScoreText(int raceTime)
    {
        mRaceTimeText.text = raceTime / 60 + ":" + raceTime % 60;
    }
}
