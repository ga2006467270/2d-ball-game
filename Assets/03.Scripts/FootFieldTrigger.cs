﻿using UnityEngine;

public class FootFieldTrigger : MonoBehaviour
{
    [SerializeField] int mTeamNumber;

    private void Start()
    {
        if (mTeamNumber == 0)
        {
            Debug.LogError("錯誤的TeamNumber");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "ball")
        {
            ScoreCounter.Instance.AddScore(mTeamNumber);
        }
    }
}
