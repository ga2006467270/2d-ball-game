﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    enum IPlayerNumber
    {
        Player1,
        Player2
    }

    [SerializeField] IPlayerNumber mPlayerNumber;
    [SerializeField] Rigidbody2D mRbody;
    [SerializeField] float mMoveForce;
    [SerializeField] float mRotateSpeed;
    KeyCode Left;
    KeyCode Right;
    KeyCode Foward;
    KeyCode Back;

    void Awake()
    {
        CheckPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void CheckPlayer()
    {
        switch (mPlayerNumber)
        {
            case IPlayerNumber.Player1:
                Left = KeyCode.A;
                Right = KeyCode.D;
                Foward = KeyCode.W;
                Back = KeyCode.S;
                break;
            case IPlayerNumber.Player2:
                Left = KeyCode.LeftArrow;
                Right = KeyCode.RightArrow;
                Foward = KeyCode.UpArrow;
                Back = KeyCode.DownArrow;
                break;
        }
    }

    void Move()
    {
        if (Input.GetKey(Left))
        {
            transform.Rotate(new Vector3(0, 0, 1), 1f * Time.deltaTime * mRotateSpeed);
        }
        if (Input.GetKey(Right))
        {
            transform.Rotate(new Vector3(0,0,1),-1f * Time.deltaTime * mRotateSpeed);
        }
        if (Input.GetKey(Foward))
        {
            mRbody.AddForce(-transform.right* mMoveForce * Time.deltaTime);
        }
        if (Input.GetKey(Back))
        {
            mRbody.AddForce(transform.right * mMoveForce * Time.deltaTime);
        }
    }
}
