﻿using UnityEngine;

public class GameManage : MonoBehaviour
{
    public static GameManage Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("Main");
                gamemanager.AddComponent<GameManage>();
            }
            return mInstance;
        }
    }

    static GameManage mInstance;

    private void Awake()
    {
        if (mInstance == null)
        {
            mInstance = this;
        }
        else
        {
            Debug.LogError("Reapeat Singleton");
            Destroy(gameObject);
        }
    }
    void Start()
    {
        ObjectPool.Instance.SetObjectPool();
        ObjectPool.Instance.RespawnGame();
    }
}
