﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreCounter : MonoBehaviour
{
    public int TeamAScore
    {
        get
        {
            return mTeamAScore;
        }

        set
        {
            mTeamAScore = value;
            ScoreCountUI.Instance.ChangeScoreUI(TeamAScore, TeamBScore);
        }
    }

    public int TeamBScore
    {
        get
        {
            return mTeamBScore;
        }

        set
        {
            mTeamBScore = value;
            ScoreCountUI.Instance.ChangeScoreUI(TeamAScore, TeamBScore);
        }
    }

    public bool StopTime
    {
        get
        {
            return mStopTime;
        }

        set
        {
            mStopTime = value;
        }
    }

    public float RaceTime
    {
        get
        {
            return mRaceTime;
        }

        set
        {
            ScoreCountUI.Instance.SetScoreText((int)mRaceTime);
            mRaceTime = value;
        }
    }

    public static ScoreCounter Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("Main");
                gamemanager.AddComponent<ScoreCounter>();
            }
            return mInstance;
        }
    }

    bool mStopTime;
    [SerializeField] float mRaceTime;
    int mTeamAScore, mTeamBScore;
    static ScoreCounter mInstance;

    void Awake()
    {
        if (mInstance == null)
        {
            mInstance = this;
        }
        else
        {
            Debug.LogError("Reapeat Singleton");
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (!StopTime)
        {
            CountRaceTime();
        }
    }

    public void AddScore(int teamNumber)
    {
        switch (teamNumber)
        {
            case 1:
                TeamAScore += 1;
                break;
            case 2:
                TeamBScore += 1;
                break;
            default:
                Debug.LogError("錯誤的Team");
                return;
        }
        StopTime = true;
        StartCoroutine(ScoreCountUI.Instance.ShowGoalText());
    }

    void CountRaceTime()
    {
        RaceTime -= Time.deltaTime;
        if (RaceTime <= 0)
        {
            Debug.Log("遊戲結束");
            Debug.Log("TeamAScore = " + TeamAScore);
            Debug.Log("TeamBScore = " + TeamBScore);
            SceneManager.LoadScene("StartScene", LoadSceneMode.Single);
        }
    }

    void GameStop()
    {
        StopTime = true;
    }
}
